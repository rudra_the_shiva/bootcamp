PLUS, MINUS, SPACE = "+", "-", " "

def mathify(s: str) -> str:
    combo = zip(reversed(signs(s)),reversed(terms(s)))
    return ''.join([''.join(_) for _ in combo])
    #return ''.join([''.join(_) for _ in zip(reversed(signs(s)), reversed(terms(s)))])


def terms(poly : str ) -> list[str]:
    return poly.replace(PLUS, SPACE).replace(MINUS , SPACE).split()


def signs(poly: str)-> str:
    if poly.strip()[0] not in PLUS + MINUS:
        poly = PLUS + poly

    return ''.join([ch for ch in poly if ch in PLUS + MINUS])

print(mathify("7x4    +     4x2  -    2"))

