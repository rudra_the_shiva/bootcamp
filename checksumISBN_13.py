def new_digits(n: int) -> int:
    def new_digit(i: int, d: str) -> int:
        if i % 2 == 0:
            return int(d + d + d)
        else: 
            return int(d)
    return [new_digit(*e) for e in enumerate(str(n))]

def check_digit(n: int) -> int:
    if (10 - (sum(new_digits(n)) % 10)) != 10:
        return 10 - (sum(new_digits(n)) % 10)
    else:
        return 0


