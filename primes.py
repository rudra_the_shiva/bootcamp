def generate_primes(limit: int) -> list[int]:
    def is_prime(n: int) -> bool:
        factor = 5
        while factor * factor <= n:
            if n % factor == 0:
                return False
            factor += 2
        return True
    
    primes = [2, 3, 5]

    for n in range(6, limit, 6):
        if n % 5 != 0:
            for step in [1, 5]:
                if is_prime(n + step):
                    primes.append(n + step)
    return primes

print(generate_primes(1000))
