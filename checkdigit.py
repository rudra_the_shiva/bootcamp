def check_digit(n: int) -> int:
    a =sum(num_to_digit(numbers_to_num(modified_number_list(num_to_digit(n)))))
    return 10 - (a % 10)

def num_to_digit(n: int) -> int:
    return [int(ch) for ch in str(n)]

def modified_number_list(digit_list: list[int]) -> int:
    digit_list_reversed = digit_list[::-1]
    number_list = []
    for n in digit_list_reversed:
        if digit_list_reversed.index(n) % 2 == 0:
            n = n * 2
            number_list.append(n)
        else:
            number_list.append(n)
    return number_list

def numbers_to_num(number_list: list[int]) -> int:
    return int(''.join([str(n) for n in number_list]))

print(check_digit(1789372997))
print(check_digit(7992739871))

