import sys
from itertools import combinations
import itertools

def make_pair(mark_student_1: list, mark_student_2: list) -> tuple[int, int]:
    return zip(mark_student_1[1:], mark_student_2[1:])

def encode(s: str) -> str:
    def encode_pair(pair: tuple[str, str]) -> bool:
       if pair[0] > pair[1]:
           return 'G'
       elif pair[0] < pair[1]:
           return 'L'
       else:
           return 'E'
   return ''.join([encode_pair(_) for _ in make_pair(s)])

def squeeze(s: str) -> str:
    if len(s) == 1:
        return s
    elif s[0] == s[1]:
        return squeeze(s[1:])
    else:
        return s[0] + squeeze(s[1:])

def make_combination(full_list: list[list[int | str]]) -> list:
    list_of_pairs = []
    combination = combinations(full_list, 2)
    for c in combination:
        list_of_pairs.append(c)
    return list_of_pairs


def get_individual_lists(list_of_pairs: list, number_of_students: int) -> list,list:

print(make_combination([['A',1,2,3],['B',3,4,5],['C',2,3,4],['D',4,5,6]]))

