def modulo_inverse_2(number: int, modulo: int) -> int | str:
    if (modulo + 1) % number == 0:
        return (modulo + 1) / number
    else:
        return "No inverse possible for this modulo"

print(modulo_inverse_2(4,35))
print(modulo_inverse_2(10,35))

