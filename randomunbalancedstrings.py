import random
from random import choices

def random_string(size: int) -> str:
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    probable_frequencies = [8.04, 1.48, 3.82, 2.51, 12.49, 2.40, 1.87, 5.05, 7.57, 0.16, 0.54, 4.07, 2.51, 7.23, 7.64, 2.14, 0.12, 6.28, 6.51, 9.28, 2.73, 1.05, 1.68, 1.66, 0.23, 0.09]
    return ''.join(ch for ch in (random.choices(alphabet, probable_frequencies, k = size))


print(random_string(500))

