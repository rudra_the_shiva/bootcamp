def is_ascending(s: str) -> bool:
    return s == ''.join(sorted(set(s)))

def is_descending(s: str) -> bool:
     return s[::-1] == ''.join(sorted(set(s)))




def is_peak(s: str) -> bool:
    first, *second = s.split(max(s))
    if len(second) == 1:
        return is_ascending(first) and is_descending(second)

def is_valley(s: str) -> bool:
    sep = max(s)
    first, _, second = s.partition(sep)
    return is_descending(first) and is_ascending(sep + second)

